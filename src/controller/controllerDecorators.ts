import { RequestHandler } from "express";
import Container from "typedi";
import CabbageApi from "../api/cabbageApi";
import { Endpoint, HttpRoute, RequestType } from "./controllerTypes";

const SubMethods = Symbol('SubMethods')

/**
 * Declares a REST API controller with the specified route.
 * 
 * @param route The base/root route of the controller. All endpoint routes within the controller will contact onto this route.
 */
export const ControllerRoute = (route: string) => {
    return function <T extends { new(...args: any[]): {} }>(Base: T) {
        return class extends Base {
            constructor(...args: any[]) {
                super(...args)
                const subMethods = Base.prototype[SubMethods];
                if (subMethods) {
                    const endpoints: Endpoint[] = []
                    subMethods.forEach((route: HttpRoute, method: string) => {
                        endpoints.push({
                            ...route,
                            handler: (this as any)[method]
                        })
                    })
                    const api = Container.get(CabbageApi)
                    api.endpointMap.set(route, endpoints)
                }
            }
        }
    }
}

const routeDecorator = (route: HttpRoute) => {
    return function routeDecorator(target: any, propertyKey: string) {
        target[SubMethods] = target[SubMethods] || new Map();
        target[SubMethods].set(propertyKey, route);
    }
}

/**
 * Declares a GET endpoint.
 * 
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export const HttpGet = (path: string, ...middleware: Array<RequestHandler>) => {
    return routeDecorator({
        path, middleware, requestType: RequestType.Get
    })
}

/**
 * Declares a POST endpoint.
 * 
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export const HttPost = (path: string, ...middleware: Array<RequestHandler>) => {
    return routeDecorator({
        path, middleware, requestType: RequestType.Post
    })
}

/**
 * Declares a PUT endpoint.
 * 
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export const HttpPut = (path: string, ...middleware: Array<RequestHandler>) => {
    return routeDecorator({
        path, middleware, requestType: RequestType.Put
    })
}

/**
 * Declares a PATCH endpoint.
 * 
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export const HttpPatch = (path: string, ...middleware: Array<RequestHandler>) => {
    return routeDecorator({
        path, middleware, requestType: RequestType.Patch
    })
}

/**
 * Declares a DELETE endpoint.
 * 
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export const HttpDelete = (path: string, ...middleware: Array<RequestHandler>) => {
    return routeDecorator({
        path, middleware, requestType: RequestType.Delete
    })
}