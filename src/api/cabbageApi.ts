import express from 'express'
import { json } from 'body-parser'
import Container, { Service } from 'typedi'
import { Endpoint, RequestType } from '../controller/controllerTypes'

@Service()
export default class CabbageApi {

    readonly app = express()

    readonly endpointMap = new Map<string, Endpoint[]>()

    /**
     * Starts the express server and listens for connections.
     */
    listen(port: number, callback?: () => void): CabbageApi {
        this.app.listen(port, callback)
        return this
    }

    /**
     * Configures the default middleware.
     * 
     * Default middlware:
     * * json (body-parser)
     * 
     * @param callback An optional callback function to configure additional middleware
     */
    configureMiddleware(callback?: Function): CabbageApi {
        this.app.use(json())

        if (callback) {
            callback()
        }

        return this
    }

    /**
     * Maps the endpoint controllers
     * 
     * @example
     * api.mapControllers([
     *  [AuthController, new AuthController()]
     * ])
     * 
     * @param entries A mapping of all the controllers
     */
    mapControllers(entries: [Function, Object][]): CabbageApi {
        for (let [func, clazz] of entries) {
            Container.set(func, clazz)
        }

        return this
    }

    /**
     * Maps the controllers endpoints to the express app
     */
    mapEndpoints(): CabbageApi {
        for (let key of this.endpointMap.keys()) {
            const endpoints = this.endpointMap.get(key)
            if (!endpoints) {
                return this
            }

            for (let endpoint of endpoints) {
                const path = `${key}${endpoint.path}`
                switch (endpoint.requestType) {
                    case RequestType.Get:
                        this.app.get(path, ...endpoint.middleware!, endpoint.handler)
                        break

                    case RequestType.Post:
                        this.app.post(path, ...endpoint.middleware!, endpoint.handler)
                        break

                    case RequestType.Put:
                        this.app.put(path, ...endpoint.middleware!, endpoint.handler)
                        break

                    case RequestType.Patch:
                        this.app.patch(path, ...endpoint.middleware!, endpoint.handler)
                        break

                    case RequestType.Delete:
                        this.app.delete(path, ...endpoint.middleware!, endpoint.handler)
                        break
                }
            }
        }

        return this
    }
}