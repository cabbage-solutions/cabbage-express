# Cabbage-Express

Cabbage-Express is a library to quickly build controller based express applications.

## Included Dependencies
| Package | Version |
|---|---|
| [Express](https://www.npmjs.com/package/express) | 4.17.3 |
| [Body-Parser](https://www.npmjs.com/package/body-parser) | 1.19.2 |
| [TypeDi](https://www.npmjs.com/package/typedi) | 0.10.0 |
| [Reflect-Metadata](https://www.npmjs.com/package/reflect-metadata) | 0.1.13 |

## Example

`$ npm install cabbage-express`

```ts
import { CabbageApi } from 'cabbage-express'

const api = Container.get(CabbageApi)

api.configureMiddleware()
.mapControllers([
    [AuthController, new AuthController()]
])
.mapEndpoints()
.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}`))
```

##### Controller

```ts
@ControllerRoute("/auth")
export default class AuthController {

    // Defines a GET request with no middleware
    @HttpGet("/login")
    async login(req: Request, res: Response) {
        ...
    }

    // Defines a POST request with the `isLoggedIn` middleware.
    @HttpPost("/change_password", isLoggedIn)
    async changePassword(req: Request, res: Response) {
        ...
    }
}
```

##### Generated endpoints 
`GET /auth/login`
`POST /auth/change_password`

## Contributing

Anyone is welcomed to contribute to Cabbage-Express. Simply clone down the repository and submit a pull-request of your desired changes.