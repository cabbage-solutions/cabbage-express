"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpDelete = exports.HttpPatch = exports.HttpPut = exports.HttPost = exports.HttpGet = exports.ControllerRoute = void 0;
const typedi_1 = __importDefault(require("typedi"));
const cabbageApi_1 = __importDefault(require("../api/cabbageApi"));
const controllerTypes_1 = require("./controllerTypes");
const SubMethods = Symbol('SubMethods');
/**
 * Declares a REST API controller with the specified route.
 *
 * @param route The base/root route of the controller. All endpoint routes within the controller will contact onto this route.
 */
const ControllerRoute = (route) => {
    return function (Base) {
        return class extends Base {
            constructor(...args) {
                super(...args);
                const subMethods = Base.prototype[SubMethods];
                if (subMethods) {
                    const endpoints = [];
                    subMethods.forEach((route, method) => {
                        endpoints.push(Object.assign(Object.assign({}, route), { handler: this[method] }));
                    });
                    const api = typedi_1.default.get(cabbageApi_1.default);
                    console.log("route:", route, "endpoints:", endpoints);
                    api.endpointMap.set(route, endpoints);
                }
            }
        };
    };
};
exports.ControllerRoute = ControllerRoute;
const routeDecorator = (route) => {
    return function routeDecorator(target, propertyKey) {
        target[SubMethods] = target[SubMethods] || new Map();
        target[SubMethods].set(propertyKey, route);
    };
};
/**
 * Declares a GET endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
const HttpGet = (path, ...middleware) => {
    return routeDecorator({
        path, middleware, requestType: controllerTypes_1.RequestType.Get
    });
};
exports.HttpGet = HttpGet;
/**
 * Declares a POST endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
const HttPost = (path, ...middleware) => {
    return routeDecorator({
        path, middleware, requestType: controllerTypes_1.RequestType.Post
    });
};
exports.HttPost = HttPost;
/**
 * Declares a PUT endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
const HttpPut = (path, ...middleware) => {
    return routeDecorator({
        path, middleware, requestType: controllerTypes_1.RequestType.Put
    });
};
exports.HttpPut = HttpPut;
/**
 * Declares a PATCH endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
const HttpPatch = (path, ...middleware) => {
    return routeDecorator({
        path, middleware, requestType: controllerTypes_1.RequestType.Patch
    });
};
exports.HttpPatch = HttpPatch;
/**
 * Declares a DELETE endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
const HttpDelete = (path, ...middleware) => {
    return routeDecorator({
        path, middleware, requestType: controllerTypes_1.RequestType.Delete
    });
};
exports.HttpDelete = HttpDelete;
//# sourceMappingURL=controllerDecorators.js.map