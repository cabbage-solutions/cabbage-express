import { RequestHandler } from "express";
export declare enum RequestType {
    Get = "GET",
    Post = "POST",
    Put = "PUT",
    Patch = "PATCH",
    Delete = "DELETE"
}
export interface HttpRoute {
    requestType: RequestType;
    path: string;
    middleware?: Array<RequestHandler>;
}
export interface Endpoint {
    requestType: RequestType;
    path: string;
    middleware?: Array<RequestHandler>;
    handler: RequestHandler;
}
