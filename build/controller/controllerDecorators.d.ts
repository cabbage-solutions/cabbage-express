import { RequestHandler } from "express";
/**
 * Declares a REST API controller with the specified route.
 *
 * @param route The base/root route of the controller. All endpoint routes within the controller will contact onto this route.
 */
export declare const ControllerRoute: (route: string) => <T extends new (...args: any[]) => {}>(Base: T) => {
    new (...args: any[]): {};
} & T;
/**
 * Declares a GET endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export declare const HttpGet: (path: string, ...middleware: Array<RequestHandler>) => (target: any, propertyKey: string) => void;
/**
 * Declares a POST endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export declare const HttPost: (path: string, ...middleware: Array<RequestHandler>) => (target: any, propertyKey: string) => void;
/**
 * Declares a PUT endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export declare const HttpPut: (path: string, ...middleware: Array<RequestHandler>) => (target: any, propertyKey: string) => void;
/**
 * Declares a PATCH endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export declare const HttpPatch: (path: string, ...middleware: Array<RequestHandler>) => (target: any, propertyKey: string) => void;
/**
 * Declares a DELETE endpoint.
 *
 * @param path The endpoint path which is contacted on the controller route
 * @param middleware An array of endpoint middleware, these are executed in order of insertion
 */
export declare const HttpDelete: (path: string, ...middleware: Array<RequestHandler>) => (target: any, propertyKey: string) => void;
