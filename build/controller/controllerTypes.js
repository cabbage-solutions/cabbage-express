"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestType = void 0;
var RequestType;
(function (RequestType) {
    RequestType["Get"] = "GET";
    RequestType["Post"] = "POST";
    RequestType["Put"] = "PUT";
    RequestType["Patch"] = "PATCH";
    RequestType["Delete"] = "DELETE";
})(RequestType = exports.RequestType || (exports.RequestType = {}));
//# sourceMappingURL=controllerTypes.js.map