import { Endpoint } from '../controller/controllerTypes';
export default class CabbageApi {
    readonly app: import("express-serve-static-core").Express;
    readonly endpointMap: Map<string, Endpoint[]>;
    /**
     * Starts the express server and listens for connections.
     */
    listen(port: number, callback?: () => void): CabbageApi;
    /**
     * Configures the default middleware.
     *
     * Default middlware:
     * * json (body-parser)
     *
     * @param callback An optional callback function to configure additional middleware
     */
    configureMiddleware(callback?: Function): CabbageApi;
    /**
     * Maps the endpoint controllers
     *
     * @example
     * api.mapControllers([
     *  [AuthController, new AuthController()]
     * ])
     *
     * @param entries A mapping of all the controllers
     */
    mapControllers(entries: [Function, Object][]): CabbageApi;
    /**
     * Maps the controllers endpoints to the express app
     */
    mapEndpoints(): CabbageApi;
}
