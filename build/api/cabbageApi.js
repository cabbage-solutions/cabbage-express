"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = require("body-parser");
const typedi_1 = __importStar(require("typedi"));
const controllerTypes_1 = require("../controller/controllerTypes");
let CabbageApi = class CabbageApi {
    constructor() {
        this.app = express_1.default();
        this.endpointMap = new Map();
    }
    /**
     * Starts the express server and listens for connections.
     */
    listen(port, callback) {
        this.app.listen(port, callback);
        return this;
    }
    /**
     * Configures the default middleware.
     *
     * Default middlware:
     * * json (body-parser)
     *
     * @param callback An optional callback function to configure additional middleware
     */
    configureMiddleware(callback) {
        this.app.use(body_parser_1.json());
        if (callback) {
            callback();
        }
        return this;
    }
    /**
     * Maps the endpoint controllers
     *
     * @example
     * api.mapControllers([
     *  [AuthController, new AuthController()]
     * ])
     *
     * @param entries A mapping of all the controllers
     */
    mapControllers(entries) {
        for (let [func, clazz] of entries) {
            typedi_1.default.set(func, clazz);
        }
        return this;
    }
    /**
     * Maps the controllers endpoints to the express app
     */
    mapEndpoints() {
        console.log("endpoints:", this.endpointMap);
        for (let key of this.endpointMap.keys()) {
            const endpoints = this.endpointMap.get(key);
            if (!endpoints) {
                console.log("no endpoints");
                return this;
            }
            for (let endpoint of endpoints) {
                console.log(endpoint);
                const path = `${key}${endpoint.path}`;
                switch (endpoint.requestType) {
                    case controllerTypes_1.RequestType.Get:
                        this.app.get(path, ...endpoint.middleware, endpoint.handler);
                        break;
                    case controllerTypes_1.RequestType.Post:
                        this.app.post(path, ...endpoint.middleware, endpoint.handler);
                        break;
                    case controllerTypes_1.RequestType.Put:
                        this.app.put(path, ...endpoint.middleware, endpoint.handler);
                        break;
                    case controllerTypes_1.RequestType.Patch:
                        this.app.patch(path, ...endpoint.middleware, endpoint.handler);
                        break;
                    case controllerTypes_1.RequestType.Delete:
                        this.app.delete(path, ...endpoint.middleware, endpoint.handler);
                        break;
                }
            }
        }
        return this;
    }
};
CabbageApi = __decorate([
    typedi_1.Service()
], CabbageApi);
exports.default = CabbageApi;
//# sourceMappingURL=cabbageApi.js.map