"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Contains various differnt HTTP status/response codes
 */
var HttpStatusCode;
(function (HttpStatusCode) {
    // ------ Successfull response codes ------
    /**
     * The request has succeeded. The information returned with the response
     * is dependent on the method used in the request, for example:
     *
     * - GET an entity corresponding to the requested resource is sent in the response;
     * - HEAD the entity-header fields corresponding to the requested resource are sent in the response without any message-body;
     * - POST an entity describing or containing the result of the action;
     * - TRACE an entity containing the request message as received by the end server.
     *
     * `Wikipedia`
     *
     * Standard response for successful HTTP requests. The actual response will depend on the request method used.
     * In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request
     * the response will contain an entity describing or containing the result of the action.
     *
     * &#9733; General status code. Most common code used to indicate success.
     */
    HttpStatusCode[HttpStatusCode["Ok"] = 200] = "Ok";
    /**
     * The request has been fulfilled and resulted in a new resource being created.
     * The newly created resource can be referenced by the URI(s) returned in the entity
     * of the response, with the most specific URI for the resource given by a Location
     * header field. The response SHOULD include an entity containing a list of resource
     * characteristics and location(s) from which the user or user agent can choose the
     * one most appropriate. The entity format is specified by the media type given in
     * the Content-Type header field. The origin server MUST create the resource before
     * returning the 201 status code. If the action cannot be carried out immediately,
     * the server SHOULD respond with 202 (Accepted) response instead.
     *
     * A 201 response MAY contain an ETag response header field indicating the current
     * value of the entity tag for the requested variant just created, see section 14.19.
     *
     * `Wikipedia`
     *
     * The request has been fulfilled and resulted in a new resource being created.
     *
     * &#9733; Successful creation occurred (via either POST or PUT). Set the Location header
     * to contain a link to the newly-created resource (on POST). Response body content
     * may or may not be present.
     */
    HttpStatusCode[HttpStatusCode["Created"] = 201] = "Created";
    /**
     * The request has been accepted for processing, but the processing has not been completed.
     * The request might or might not eventually be acted upon, as it might be disallowed when
     * processing actually takes place. There is no facility for re-sending a status code from
     * an asynchronous operation such as this.
     *
     * The 202 response is intentionally non-committal. Its purpose is to allow a server to accept a
     * request for some other process (perhaps a batch-oriented process that is only run once per day)
     * without requiring that the user agent's connection to the server persist until the process is completed.
     * The entity returned with this response SHOULD include an indication of the request's current status and
     * either a pointer to a status monitor or some estimate of when the user can expect the request to be fulfilled.
     *
     * `Wikipedia`
     *
     * The request has been accepted for processing, but the processing has not been completed. The request might
     * or might not eventually be acted upon, as it might be disallowed when processing actually takes place.
     */
    HttpStatusCode[HttpStatusCode["Accepted"] = 202] = "Accepted";
    /**
     * The returned metainformation in the entity-header is not the definitive set as available from the origin server,
     * but is gathered from a local or a third-party copy. The set presented MAY be a subset or superset of the original
     * version. For example, including local annotation information about the resource might result in a superset of the
     * metainformation known by the origin server. Use of this response code is not required and is only appropriate when
     * the response would otherwise be 200 (OK).
     *
     * `Wikipedia`
     *
     * The server successfully processed the request, but is returning information that may be from another source.
     *
     * Not present in HTTP/1.0: available since HTTP/1.1
     */
    HttpStatusCode[HttpStatusCode["NonAuthoritativeInformation"] = 203] = "NonAuthoritativeInformation";
    /**
     * The server has fulfilled the request but does not need to return an entity-body, and might want to return updated metainformation.
     * The response MAY include new or updated metainformation in the form of entity-headers, which if present SHOULD be associated with
     * the requested variant.
     *
     * If the client is a user agent, it SHOULD NOT change its document view from that which caused the request to be sent. This response is
     * primarily intended to allow input for actions to take place without causing a change to the user agent's active document view, although
     * any new or updated metainformation SHOULD be applied to the document currently in the user agent's active view.
     *
     * The 204 response MUST NOT include a message-body, and thus is always terminated by the first empty line after the header fields.
     *
     * `Wikipedia`
     *
     * The server successfully processed the request, but is not returning any content.
     *
     * &#9733; Status when wrapped responses (e.g. JSEND) are not used and nothing is in the body (e.g. DELETE).
     */
    HttpStatusCode[HttpStatusCode["NoContent"] = 204] = "NoContent";
    /**
     * The server has fulfilled the request and the user agent SHOULD reset the document view which caused the request to be sent.
     * This response is primarily intended to allow input for actions to take place via user input, followed by a clearing of the form
     * in which the input is given so that the user can easily initiate another input action. The response MUST NOT include an entity.
     *
     * `Wikipedia`
     *
     * The server successfully processed the request, but is not returning any content. Unlike a 204 response, this response requires that
     * the requester reset the document view.
     */
    HttpStatusCode[HttpStatusCode["ResetContent"] = 205] = "ResetContent";
    HttpStatusCode[HttpStatusCode["PartialContent"] = 206] = "PartialContent";
    HttpStatusCode[HttpStatusCode["MultiStatus"] = 207] = "MultiStatus";
    HttpStatusCode[HttpStatusCode["AlreadyReported"] = 208] = "AlreadyReported";
    HttpStatusCode[HttpStatusCode["IMUsed"] = 226] = "IMUsed";
    // ------ redirect response codes ------
    HttpStatusCode[HttpStatusCode["MultipleChoices"] = 300] = "MultipleChoices";
    HttpStatusCode[HttpStatusCode["MovedPermanently"] = 301] = "MovedPermanently";
    HttpStatusCode[HttpStatusCode["Found"] = 302] = "Found";
    HttpStatusCode[HttpStatusCode["SeeOther"] = 303] = "SeeOther";
    HttpStatusCode[HttpStatusCode["NotModified"] = 304] = "NotModified";
    HttpStatusCode[HttpStatusCode["UseProxy"] = 305] = "UseProxy";
    HttpStatusCode[HttpStatusCode["TemporaryRedirect"] = 307] = "TemporaryRedirect";
    HttpStatusCode[HttpStatusCode["PermanentRedirect"] = 308] = "PermanentRedirect";
    // ------ client error response codes ------
    HttpStatusCode[HttpStatusCode["BadRequest"] = 400] = "BadRequest";
    HttpStatusCode[HttpStatusCode["Unauthorized"] = 401] = "Unauthorized";
    HttpStatusCode[HttpStatusCode["PaymentRequired"] = 402] = "PaymentRequired";
    HttpStatusCode[HttpStatusCode["Forbidden"] = 403] = "Forbidden";
    HttpStatusCode[HttpStatusCode["NotFound"] = 404] = "NotFound";
    HttpStatusCode[HttpStatusCode["MethodNotAllowed"] = 405] = "MethodNotAllowed";
    HttpStatusCode[HttpStatusCode["NotAcceptable"] = 406] = "NotAcceptable";
    HttpStatusCode[HttpStatusCode["ProxyAuthenticationRequired"] = 407] = "ProxyAuthenticationRequired";
    HttpStatusCode[HttpStatusCode["RequestTimeout"] = 408] = "RequestTimeout";
    HttpStatusCode[HttpStatusCode["Conflict"] = 409] = "Conflict";
    HttpStatusCode[HttpStatusCode["Gone"] = 410] = "Gone";
    HttpStatusCode[HttpStatusCode["LengthRequired"] = 411] = "LengthRequired";
    // ------ server error response codes ------
    HttpStatusCode[HttpStatusCode["InternalServerError"] = 500] = "InternalServerError";
    HttpStatusCode[HttpStatusCode["NotImplemented"] = 501] = "NotImplemented";
    HttpStatusCode[HttpStatusCode["BadGateway"] = 502] = "BadGateway";
    HttpStatusCode[HttpStatusCode["ServiceUnavailable"] = 503] = "ServiceUnavailable";
    HttpStatusCode[HttpStatusCode["GatewayTimeout"] = 504] = "GatewayTimeout";
})(HttpStatusCode || (HttpStatusCode = {}));
exports.default = HttpStatusCode;
//# sourceMappingURL=httpStatusCode.js.map