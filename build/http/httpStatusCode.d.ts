/**
 * Contains various differnt HTTP status/response codes
 */
declare enum HttpStatusCode {
    /**
     * The request has succeeded. The information returned with the response
     * is dependent on the method used in the request, for example:
     *
     * - GET an entity corresponding to the requested resource is sent in the response;
     * - HEAD the entity-header fields corresponding to the requested resource are sent in the response without any message-body;
     * - POST an entity describing or containing the result of the action;
     * - TRACE an entity containing the request message as received by the end server.
     *
     * `Wikipedia`
     *
     * Standard response for successful HTTP requests. The actual response will depend on the request method used.
     * In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request
     * the response will contain an entity describing or containing the result of the action.
     *
     * &#9733; General status code. Most common code used to indicate success.
     */
    Ok = 200,
    /**
     * The request has been fulfilled and resulted in a new resource being created.
     * The newly created resource can be referenced by the URI(s) returned in the entity
     * of the response, with the most specific URI for the resource given by a Location
     * header field. The response SHOULD include an entity containing a list of resource
     * characteristics and location(s) from which the user or user agent can choose the
     * one most appropriate. The entity format is specified by the media type given in
     * the Content-Type header field. The origin server MUST create the resource before
     * returning the 201 status code. If the action cannot be carried out immediately,
     * the server SHOULD respond with 202 (Accepted) response instead.
     *
     * A 201 response MAY contain an ETag response header field indicating the current
     * value of the entity tag for the requested variant just created, see section 14.19.
     *
     * `Wikipedia`
     *
     * The request has been fulfilled and resulted in a new resource being created.
     *
     * &#9733; Successful creation occurred (via either POST or PUT). Set the Location header
     * to contain a link to the newly-created resource (on POST). Response body content
     * may or may not be present.
     */
    Created = 201,
    /**
     * The request has been accepted for processing, but the processing has not been completed.
     * The request might or might not eventually be acted upon, as it might be disallowed when
     * processing actually takes place. There is no facility for re-sending a status code from
     * an asynchronous operation such as this.
     *
     * The 202 response is intentionally non-committal. Its purpose is to allow a server to accept a
     * request for some other process (perhaps a batch-oriented process that is only run once per day)
     * without requiring that the user agent's connection to the server persist until the process is completed.
     * The entity returned with this response SHOULD include an indication of the request's current status and
     * either a pointer to a status monitor or some estimate of when the user can expect the request to be fulfilled.
     *
     * `Wikipedia`
     *
     * The request has been accepted for processing, but the processing has not been completed. The request might
     * or might not eventually be acted upon, as it might be disallowed when processing actually takes place.
     */
    Accepted = 202,
    /**
     * The returned metainformation in the entity-header is not the definitive set as available from the origin server,
     * but is gathered from a local or a third-party copy. The set presented MAY be a subset or superset of the original
     * version. For example, including local annotation information about the resource might result in a superset of the
     * metainformation known by the origin server. Use of this response code is not required and is only appropriate when
     * the response would otherwise be 200 (OK).
     *
     * `Wikipedia`
     *
     * The server successfully processed the request, but is returning information that may be from another source.
     *
     * Not present in HTTP/1.0: available since HTTP/1.1
     */
    NonAuthoritativeInformation = 203,
    /**
     * The server has fulfilled the request but does not need to return an entity-body, and might want to return updated metainformation.
     * The response MAY include new or updated metainformation in the form of entity-headers, which if present SHOULD be associated with
     * the requested variant.
     *
     * If the client is a user agent, it SHOULD NOT change its document view from that which caused the request to be sent. This response is
     * primarily intended to allow input for actions to take place without causing a change to the user agent's active document view, although
     * any new or updated metainformation SHOULD be applied to the document currently in the user agent's active view.
     *
     * The 204 response MUST NOT include a message-body, and thus is always terminated by the first empty line after the header fields.
     *
     * `Wikipedia`
     *
     * The server successfully processed the request, but is not returning any content.
     *
     * &#9733; Status when wrapped responses (e.g. JSEND) are not used and nothing is in the body (e.g. DELETE).
     */
    NoContent = 204,
    /**
     * The server has fulfilled the request and the user agent SHOULD reset the document view which caused the request to be sent.
     * This response is primarily intended to allow input for actions to take place via user input, followed by a clearing of the form
     * in which the input is given so that the user can easily initiate another input action. The response MUST NOT include an entity.
     *
     * `Wikipedia`
     *
     * The server successfully processed the request, but is not returning any content. Unlike a 204 response, this response requires that
     * the requester reset the document view.
     */
    ResetContent = 205,
    PartialContent = 206,
    MultiStatus = 207,
    AlreadyReported = 208,
    IMUsed = 226,
    MultipleChoices = 300,
    MovedPermanently = 301,
    Found = 302,
    SeeOther = 303,
    NotModified = 304,
    UseProxy = 305,
    TemporaryRedirect = 307,
    PermanentRedirect = 308,
    BadRequest = 400,
    Unauthorized = 401,
    PaymentRequired = 402,
    Forbidden = 403,
    NotFound = 404,
    MethodNotAllowed = 405,
    NotAcceptable = 406,
    ProxyAuthenticationRequired = 407,
    RequestTimeout = 408,
    Conflict = 409,
    Gone = 410,
    LengthRequired = 411,
    InternalServerError = 500,
    NotImplemented = 501,
    BadGateway = 502,
    ServiceUnavailable = 503,
    GatewayTimeout = 504
}
export default HttpStatusCode;
